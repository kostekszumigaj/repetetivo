var xhr = new XMLHttpRequest();

xhr.onload = function() {
    if(xhr.status === 200){
        responseObject = JSON.parse(xhr.responseText);
        
        var newContent = '';
        for (var i = 0; i < responseObject.songs.length; i++)
        {
            newContent += '<div class="song">';
            newContent += '<img src="' + responseObject.songs[i].cover + '" ';
            newContent += 'alt="' + responseObject.songs[i].title + ' - ' + responseObject.songs[i].artist + '" />';
            newContent += '<p><b>' + responseObject.songs[i].title + '</b><br>';
            newContent += responseObject.songs[i].artist + '</p>';
            newContent += '</div>';
        }
        
        document.getElementsById("songs").innerHTML = newContent;
        
        
    }
};
    
xhr.open('GET', 'data.json', true);
xhr.send(null);


